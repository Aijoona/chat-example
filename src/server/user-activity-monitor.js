const EventEmitter = require('events');

class UserActivityMonitor extends EventEmitter {
    constructor(activityWindowInSeconds) {
        if (!activityWindowInSeconds) {
            throw new Error('missingArgument: activityWindowInSeconds');
        }
        super();
        this.activityWindowInSeconds = activityWindowInSeconds;
        this.activeUsers = {};
        this.timeouts = {};
    }

    getValueHash() {
        return Object.keys(this.activeUsers).sort().join('|');
    }

    add(user) {
        const {id} = user;
        const originalValueHash = this.getValueHash();
        const beforeValue = this.getActiveUsers();

        if (this.activeUsers[id]) {
            clearTimeout(this.timeouts[id]);
        }

        this.activeUsers[id] = user;
        this.timeouts[id] = setTimeout(() => this.remove(id), this.activityWindowInSeconds * 1000);

        if (this.getValueHash() !== originalValueHash) {
            this.emit('change', this.getActiveUsers() || [], beforeValue || []);
        }
    }

    getActiveUsers() {
        return Object.values(this.activeUsers || {});
    }

    remove(id) {
        if (this.timeouts[id]) {
            const beforeValue = this.getActiveUsers();
            clearTimeout(this.timeouts[id]);
            delete this.timeouts[id];
            delete this.activeUsers[id];
            this.emit('change', this.getActiveUsers() || [], beforeValue || []);
        }
    }
}

module.exports = {UserActivityMonitor};
