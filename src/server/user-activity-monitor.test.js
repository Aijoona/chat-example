import {UserActivityMonitor} from "./user-activity-monitor";
import {EventEmitter} from "events";
import {User} from "../model/user";


describe('UserActivityMonitor', () => {
    describe('#constructor', () => {
        it('requires "activityWindowInSeconds" parameter', () => {
            expect(() => {
                new UserActivityMonitor();
            }).toThrow('missingArgument: activityWindowInSeconds');
        });

        it('should implements EventEmitter', () => {
            expect(new UserActivityMonitor(10)).toBeInstanceOf(EventEmitter);
        });

        it('should have no initial value', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            expect(userActivityMonitor.getValueHash()).toBe('');
        });
    });

    describe('#add', () => {
        it('should change the value when adding users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            const firstValue = userActivityMonitor.getValueHash();

            userActivityMonitor.add(new User('Ruben'));

            const secondValue = userActivityMonitor.getValueHash();

            expect(firstValue).not.toBe(secondValue);

            userActivityMonitor.add(new User('Martha'));

            expect(secondValue).not.toBe(userActivityMonitor.getValueHash());
        });

        it('should emit "change" event when adding users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            let count = 0;
            userActivityMonitor.emit = () => {
                count++;
            };

            expect(count).toBe(0);

            const ruben = new User('Ruben');
            const martha = new User('Martha');

            userActivityMonitor.add(ruben);
            expect(count).toBe(1);
            userActivityMonitor.add(martha);
            expect(count).toBe(2);
        });

        it('should not emit "change" event when adding already added users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            let count = 0;
            userActivityMonitor.emit = () => {
                count++;
            };

            const ruben = new User('Ruben');

            expect(count).toBe(0);

            userActivityMonitor.add(ruben);

            expect(count).toBe(1);

            userActivityMonitor.add(ruben);
            userActivityMonitor.add(ruben);
            userActivityMonitor.add(ruben);

            expect(count).toBe(1);
        });

        it('should not emit "change" event when adding already added users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            let count = 0;
            userActivityMonitor.emit = () => {
                count++;
            };

            const ruben = new User('Ruben');

            expect(count).toBe(0);

            userActivityMonitor.add(ruben);

            expect(count).toBe(1);

            userActivityMonitor.add(ruben);
            userActivityMonitor.add(ruben);
            userActivityMonitor.add(ruben);

            expect(count).toBe(1);
        });
    });

    describe('#remove', () => {
        it('should change the value when removing users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);

            const ruben = new User('Ruben');
            const martha = new User('Martha');
            userActivityMonitor.add(ruben);
            userActivityMonitor.add(martha);

            const firstValue = userActivityMonitor.getValueHash();

            expect(firstValue).toBeTruthy();

            userActivityMonitor.remove(ruben.id);

            expect(firstValue).not.toBe(userActivityMonitor.getValueHash());
        });

        it('should emit "change" event when removing users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            let count = 0;
            userActivityMonitor.emit = () => {
                count++;
            };

            const ruben = new User('Ruben');
            const martha = new User('Martha');

            expect(count).toBe(0);

            userActivityMonitor.add(ruben);
            userActivityMonitor.add(martha);

            expect(count).toBe(2);

            userActivityMonitor.remove(ruben.id);

            expect(count).toBe(3);

            userActivityMonitor.remove(martha.id);

            expect(count).toBe(4);
        });

        it('should not emit "change" event when removing already removed users', () => {
            const userActivityMonitor = new UserActivityMonitor(10);
            let count = 0;
            userActivityMonitor.emit = () => {
                count++;
            };

            const ruben = new User('Ruben');

            expect(count).toBe(0);

            userActivityMonitor.add(ruben);

            expect(count).toBe(1);

            userActivityMonitor.remove(ruben.id);
            userActivityMonitor.remove(ruben.id);
            userActivityMonitor.remove(ruben.id);

            expect(count).toBe(2);
        });
    });
});





