import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
    Grid,
    AppBar,
    Toolbar,
    Typography,
    Button,
    Container,
} from '@material-ui/core';
import {Chat} from "./chat";
import PropTypes from "prop-types";
import {useChatStore} from "./hook/chat-store";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    chatGrid: {
        marginTop: 10,
    },
    title: {
        flexGrow: 1,
    },
}));

function AppToolbar({onSpawnChatClick}) {
    const classes = useStyles();
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    Chat Example
                </Typography>
                <Button color="inherit" onClick={onSpawnChatClick}>
                    Spawn chat
                </Button>
            </Toolbar>
        </AppBar>
    );
}

AppToolbar.defaultProps = {
    onSpawnChatClick: PropTypes.func.isRequired,
};

function App() {
    const classes = useStyles();
    const {
        chats,
        createChat,
        removeChat,
        addMessage,
        messages,
        onUserActivity,
        activeUsers,
        connectedUsers,
        heartBeat,
        onPluginToggle,
    } = useChatStore();

    const addChat = () => {
        const name = prompt('Enter a username');
        if (name === null) {
            return;
        }
        if (name) {
            createChat(name);
        } else {
            addChat();
        }
    };

    useEffect(() => {
        const heartBeatInterval = setInterval(
            () => heartBeat(chats.map(({user}) => user), 5 * 1000)
        );
        return () => clearInterval(heartBeatInterval);
    }, [chats, heartBeat]);

    useEffect(() => {
        createChat('Rob');
    }, []); // Hackish? way to emulate component mount hook

    return (
        <div className={classes.root}>
            <Container maxWidth="xl">
                <AppToolbar onSpawnChatClick={addChat}/>
                <Grid container spacing={1} className={classes.chatGrid}>
                    {chats.map((chat, index, chats) => (
                        <Grid
                            item
                            lg={12}
                            xl={index === chats.length - 1 && chats.length % 2 === 1 ? 12 : 6}
                            key={chat.id}
                        >
                            <Chat
                                chat={chat}
                                onPluginToggle={onPluginToggle}
                                users={connectedUsers}
                                onChatRemoveClick={() => removeChat(chat.id)}
                                onMessageSend={(text) => addMessage(chat.user, text)}
                                messages={messages}
                                onUserActivity={() => onUserActivity(chat.user)}
                                activeUsers={activeUsers.filter(({id}) => chat.user.id !== id)}
                            />
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </div>
    );
}

export default App;
