const {UserActivityMonitor} = require('./server/user-activity-monitor');
const {eventType} = require('./event-type');

var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

const userActivityMonitor = new UserActivityMonitor(10);
const userConnectionMonitor = new UserActivityMonitor(30);

const emit = (event) => {
    console.log('Emiting: ', event);
    io.emit('event', event);
};

io.on('connection', socket => {
    let onUserActivityMonitorChange = (users) => {
        emit({type: eventType.USER_ACTIVITY_CHANGE, payload: {users}});
    };
    let onUserConnectionMonitorChange = (afterValue, beforeValue) => {
        const afterValueIds = afterValue.map(({id}) => id);
        const beforeValueIds = beforeValue.map(({id}) => id);
        const connected = afterValue.filter(({id}) => beforeValueIds.indexOf(id) === -1);
        const disconnected = beforeValue.filter(({id}) => afterValueIds.indexOf(id) === -1);

        if (connected.length) {
            connected.forEach((user) => emit({
                type: eventType.USER_CONNECTED,
                payload: {user, connectedUsers: afterValue}
            }));
        }

        if (disconnected.length) {
            disconnected.forEach((user) => emit({
                type: eventType.USER_DISCONNECTED,
                payload: {user, connectedUsers: afterValue}
            }));
        }

    };

    userActivityMonitor.on('change', onUserActivityMonitorChange);
    userConnectionMonitor.on('change', onUserConnectionMonitorChange);

    socket.on('event', event => {
        const {type, payload} = event;
        switch (type) {
            case eventType.USER_TYPING:
                userActivityMonitor.add(payload.user);
                break;
            case eventType.USER_HEART_BEAT:
                payload.usersIds.forEach((id) => userConnectionMonitor.add(id));
                break;
            case eventType.USER_CONNECTED:
                userConnectionMonitor.add(payload.user);
                break;
            case eventType.USER_DISCONNECTED:
                userConnectionMonitor.remove(payload.user);
                break;
            default:
                emit(event);
                break;
        }
    });

    socket.on('disconnect', (reason) => {
        userActivityMonitor.off('change', onUserActivityMonitorChange);
        userConnectionMonitor.off('change', onUserConnectionMonitorChange);
    });
});

http.listen(4000, () => {
    console.log('listening on *:4000');
});
