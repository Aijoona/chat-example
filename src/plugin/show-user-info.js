import React from 'react';
import {Plugin, PluginManager} from "./plugin-manager";
import {UserConnectedMessage} from "../model/user-connected-message";
import {UserDisconnectedMessage} from "../model/user-disconnected-message";
import {makeStyles} from "@material-ui/core/styles";
import {Avatar, Tooltip} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    tooltipTrigger: {
        cursor: 'pointer'
    }
}));

function UserInfoTooltip({ReactNode, message}) {
    const classes = useStyles();
    if (message instanceof UserConnectedMessage || message instanceof UserDisconnectedMessage) {
        return ReactNode;
    }
    const {user} = message;
    return (
        <Tooltip
            placement="bottom-start"
            title={
                <React.Fragment>
                    <div className={classes.tooltipTrigger}>
                        <Avatar alt="Profile Picture" src={user.avatar}/>
                        <p>{user.name}</p>
                        <p>{user.id}</p>
                    </div>
                </React.Fragment>
            }
        >
            <div>
                {ReactNode}
            </div>
        </Tooltip>
    );
}

export class ShowUserInfo extends Plugin {
    constructor() {
        super({
            [PluginManager.MESSAGE_LIST_USER_NAME]: (ReactNode, message) => this.addUserInfo(ReactNode, message)
        });
    }

    addUserInfo(ReactNode, message) {
        return <UserInfoTooltip ReactNode={ReactNode} message={message}/>
    }

    getName() {
        return 'Messages: Show user info';
    }
}
