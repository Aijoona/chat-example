import React from 'react';
import {Plugin, PluginManager} from "./plugin-manager";
import {Avatar, ListItemAvatar, ListItem, ListItemText} from "@material-ui/core";


function UserListAvatarDecorator({user}) {
    return (
        <ListItem alignItems="flex-start">
            <ListItemAvatar>
                <Avatar alt="User avatar" src={user.avatar}/>
            </ListItemAvatar>
            <ListItemText primary={user.name} />
        </ListItem>
    );
}

export class AddAvatarUserList extends Plugin {
    constructor() {
        super({
            [PluginManager.USER_LIST_ENTRY]: (ReactNode, user) => this.addUserAvatar(ReactNode, user)
        });
    }

    addUserAvatar(ReactNode, user) {
        return <UserListAvatarDecorator user={user}/>
    }

    getName() {
        return 'User list: Add avatar';
    }
}
