export class PluginManager {
    constructor(plugins = []) {
        this.plugins = plugins;
    }

    exec(action, initialValue, ...args) {
        return this.plugins
            .filter(({ enabled }) => enabled)
            .reduce((result, plugin) => {
                return plugin.exec(action, result, ...args);
            }, initialValue);
    }

    toggle(pluginName) {
        this.plugins.forEach((plugin) => {
            if (plugin.getName() === pluginName) {
                plugin.toggle();
            }
        })
    }
}

export class Plugin {
    constructor(handlers) {
        this.handlers = handlers;
        this.enabled = true;
    }

    getName() {
        return 'Unnamed plugin';
    }

    exec(action, initialValue, ...args) {
        if (this.handlers[action]) {
            return this.handlers[action](initialValue, ...args);
        }
        return initialValue;
    }

    toggle() {
        this.enabled = !this.enabled;
    }
}

PluginManager.MESSAGE_LIST_USER_NAME = 'MESSAGE_LIST_USER_NAME';
PluginManager.USER_LIST_ENTRY = 'USER_LIST_ENTRY';
