import React from 'react';
import {Plugin, PluginManager} from "./plugin-manager";
import {UserConnectedMessage} from "../model/user-connected-message";
import {UserDisconnectedMessage} from "../model/user-disconnected-message";
import moment from "moment";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    timestamp: {
        fontSize: '90%',
        color: '#777',
        marginLeft: 5,
    }
}));

function MessageTimestamp({ ReactNode, message }) {
    const classes = useStyles();
    if (message instanceof UserConnectedMessage || message instanceof UserDisconnectedMessage) {
        return ReactNode;
    }
    return (
        <React.Fragment>
            {ReactNode}
            <span className={classes.timestamp}>{moment(message.time).format('h:mm')}</span>
        </React.Fragment>
    );
}

export class ShowTimestamp extends Plugin {
    constructor() {
        super({
            [PluginManager.MESSAGE_LIST_USER_NAME]: (ReactNode, message) => this.addTimestamp(ReactNode, message)
        });
    }

    addTimestamp(ReactNode, message) {
        return <MessageTimestamp ReactNode={ReactNode} message={message} />
    }

    getName() {
        return 'Messages: Show timestamp';
    }
}
