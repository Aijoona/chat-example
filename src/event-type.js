const eventType = {
    MESSAGE_SENT: 'message_sent',
    USER_CONNECTED: 'user_connected',
    USER_DISCONNECTED: 'user_disconnected',
    USER_TYPING: 'user_typing',
    USER_ACTIVITY_CHANGE: 'user_activity_change',
    USER_HEART_BEAT: 'user_heart_beat,'
};

module.exports = {eventType};
