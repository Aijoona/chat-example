import {makeStyles} from "@material-ui/core/styles";
import React, {useEffect, useRef} from "react";
import {Avatar, List, ListItem, ListItemAvatar, ListItemText, Paper} from "@material-ui/core";
import PropTypes from "prop-types";
import {PluginManager} from "../plugin/plugin-manager";

const useStyles = makeStyles(theme => ({
    paper: {
        overflowY: 'scroll',
        paddingBottom: 5,
        height: 'calc(100% - 89px)',
    },
    chatBody: {
        position: 'relative',
        height: '100%',
    },
}));

function Message({chat, message}) {
    const {user, text} = message;
    const UserNameNode = chat.pluginManager.exec(PluginManager.MESSAGE_LIST_USER_NAME, user.name, message);
    return (
        <ListItem>
            {
                user.avatar ?
                    <ListItemAvatar>
                        <Avatar alt="Profile Picture" src={user.avatar}/>
                    </ListItemAvatar> :
                    null
            }
            <ListItemText primary={UserNameNode} secondary={text}/>
        </ListItem>
    );
}

export function MessageList({chat, messages}) {
    const classes = useStyles();
    const scrollAnchor = useRef(null);
    useEffect(() => {
        scrollAnchor.current.scrollIntoView();
    });
    return (
        <Paper square className={classes.paper}>
            <List className={classes.list}>
                {messages.map((message) => <Message chat={chat} message={message} key={message.id}/>)}
            </List>
            <div ref={scrollAnchor}/>
        </Paper>
    );
}

MessageList.propTypes = {
    chat: PropTypes.object.isRequired,
    messages: PropTypes.array.isRequired,
};
