import React, {useEffect, useRef, useState} from "react";
import {AppBar, TextField, Toolbar} from "@material-ui/core";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    activeUsers: {
        paddingTop: 5,
        paddingLeft: 25,
        fontStyle: 'italic',
        color: '#888',
        fontSize: '85%',
        height: 19,
    },
}));


function ActiveUsers({activeUsers}) {
    const classes = useStyles();
    const names = activeUsers.map(({name}) => name);

    if (names.length === 0) {
        return <div className={classes.activeUsers}/>;
    }

    let message;

    if (names.length === 1) {
        message = `${names[0]} is writing...`
    } else {
        const last = names.pop();
        const joined = names.join(', ');
        message = `${joined} and ${last} are writing...`
    }

    return (
        <div className={classes.activeUsers}>
            {message}
        </div>
    );
}

export function Input({onMessageSend, onUserActivity, activeUsers}) {
    const classes = useStyles();
    const inputRef = useRef(null);
    const [value, setValue] = useState('');

    const onKeyPress = ({key}) => {
        const value = inputRef.current.value;
        if (key === 'Enter' && value) {
            onMessageSend(value);
            setValue('');
        }
        onUserActivity();
    };

    useEffect(() => {
        let input = inputRef.current;
        input.addEventListener('keyup', onKeyPress);
        return () => {
            input.removeEventListener('keyup', onKeyPress);
        };
    });

    return (
        <AppBar position="static" color="default" className={classes.appBar} elevation={0}>
            <ActiveUsers activeUsers={activeUsers}/>
            <Toolbar>
                <TextField
                    fullWidth
                    placeholder="Type here"
                    inputRef={inputRef}
                    value={value}
                    onChange={({target}) => setValue(target.value)}
                />
            </Toolbar>
        </AppBar>
    );
}

Input.propTypes = {
    onMessageSend: PropTypes.func.isRequired,
    onUserActivity: PropTypes.func.isRequired,
    activeUsers: PropTypes.array.isRequired,
};
