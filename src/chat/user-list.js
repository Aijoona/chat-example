import {List, ListItem, ListItemText} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import {PluginManager} from "../plugin/plugin-manager";

const useStyles = makeStyles(theme => ({
    chatDrawer: {
        height: 'calc(100% - 10px)',
        overflowY: 'auto',
        paddingBottom: 5,
        paddingTop: 5,
    },
}));

function UserListItem({user, chat}) {
    const initialValue = (
        <ListItem>
            <ListItemText primary={user.name}/>
        </ListItem>
    );
    return chat.pluginManager.exec(PluginManager.USER_LIST_ENTRY, initialValue, user);
}

UserListItem.defaultProps = {
    chat: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
};

export function UserList({users, chat}) {
    const classes = useStyles();
    return (
        <List className={classes.chatDrawer}>
            {users.map((user) => (
                <UserListItem key={user.id} user={user} chat={chat}/>
            ))}
        </List>
    );
}

UserList.defaultProps = {
    chat: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
};
