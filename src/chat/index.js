import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Container} from '@material-ui/core';
import {Header} from './header';
import {UserList} from "./user-list";
import {Body} from "./body";
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
    chatWrapper: {
        height: 500,
        marginBottom: 50,
        overflow: 'hidden',
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    limitHeight: {
        height: '100%',
        padding: 0,
    },
    rightPanel: {
        height: 'calc(100% - 60px)',
    },
}));


export function Chat({
     chat,
     users,
     messages,
     onChatRemoveClick,
     onMessageSend,
     onUserActivity,
     activeUsers,
     onPluginToggle
}) {
    const classes = useStyles();
    return (
        <Container className={classes.chatWrapper}>
            <Header user={chat.user} chat={chat} onChatRemoveClick={onChatRemoveClick} onPluginToggle={onPluginToggle}/>
            <Grid container spacing={1} className={classes.rightPanel}>
                <Grid item xs={3} className={classes.limitHeight}>
                    <UserList users={users} chat={chat}/>
                </Grid>
                <Grid item xs={9} className={classes.limitHeight}>
                    <Body
                        chat={chat}
                        messages={messages}
                        onMessageSend={onMessageSend}
                        onUserActivity={onUserActivity}
                        activeUsers={activeUsers}
                    />
                </Grid>
            </Grid>
        </Container>
    );
}

Chat.propTypes = {
    chat: PropTypes.object.isRequired,
    messages: PropTypes.array.isRequired,
    users: PropTypes.array.isRequired,
    onChatRemoveClick: PropTypes.func.isRequired,
    onMessageSend: PropTypes.func.isRequired,
    onUserActivity: PropTypes.func.isRequired,
    onPluginToggle: PropTypes.func.isRequired,
    activeUsers: PropTypes.array.isRequired,
};
