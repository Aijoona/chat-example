import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from 'prop-types';
import {Input} from "./input";
import {MessageList} from "./message-list";

const useStyles = makeStyles(theme => ({
    chatBody: {
        position: 'relative',
        height: '100%',
    },
}));

export function Body({chat, messages, onMessageSend, onUserActivity, activeUsers}) {
    const classes = useStyles();
    return (
        <div className={classes.chatBody}>
            <MessageList
                chat={chat}
                messages={messages}
            />
            <Input
                onMessageSend={onMessageSend}
                onUserActivity={onUserActivity}
                activeUsers={activeUsers}
            />
        </div>
    );
}

Body.propTypes = {
    chat: PropTypes.object.isRequired,
    messages: PropTypes.array.isRequired,
    onMessageSend: PropTypes.func.isRequired,
    onUserActivity: PropTypes.func.isRequired,
    activeUsers: PropTypes.array.isRequired,
};
