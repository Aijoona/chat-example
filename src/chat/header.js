import {AppBar, Button, Menu, Toolbar, Typography, MenuItem, FormControlLabel, Checkbox} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';
import React, {useState} from "react";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
    title: {
        flexGrow: 1,
    },
}));

export function Header({ user, chat, onChatRemoveClick, onPluginToggle }) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const plugins = chat.pluginManager.plugins;

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    Chat Instance ({ user.name })
                </Typography>
                <Button color="inherit" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                    Plugins
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    {
                        plugins.map((plugin) => (
                            <MenuItem key={plugin.getName()}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={plugin.enabled}
                                            color="primary"
                                            onChange={() => onPluginToggle(chat, plugin.getName())}
                                        />
                                    }
                                    label={plugin.getName()}
                                />
                            </MenuItem>
                        ))
                    }
                </Menu>
                <Button color="inherit" onClick={onChatRemoveClick}>Close</Button>
            </Toolbar>
        </AppBar>
    );
}

Header.defaultProps = {
    chat: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    onChatRemoveClick: PropTypes.func.isRequired,
    onPluginToggle: PropTypes.func.isRequired,
};
