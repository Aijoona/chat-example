import {uuid} from "uuidv4";

export class Chat {
    constructor(user, pluginManager) {
        this.id = uuid();
        this.user = user;
        this.pluginManager = pluginManager;
    }
}
