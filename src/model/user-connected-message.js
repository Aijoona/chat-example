import {uuid} from "uuidv4";

export class UserConnectedMessage {
    constructor({ name }) {
        this.id = uuid();
        this.user = { name: '', avatar: null };
        this.text = name + ' joined the conversation';
        this.time = new Date();
    }
}
