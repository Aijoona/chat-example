import {uuid} from "uuidv4";

export class User {
    constructor(name) {
        this.id = uuid();
        this.name = name;
        const sanitizedName = name.replace(/[^a-z0-9]/gi, '');
        this.avatar = `https://api.adorable.io/avatars/120/${sanitizedName}.png`;
    }
}
