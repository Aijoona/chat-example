import {uuid} from "uuidv4";

export class UserDisconnectedMessage {
    constructor({ name }) {
        this.id = uuid();
        this.user = { name: '', avatar: null };
        this.text = name + ' left the conversation';
        this.time = new Date();
    }
}
