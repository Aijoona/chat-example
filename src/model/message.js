import {uuid} from "uuidv4";

export class Message {
    constructor(user, text) {
        this.id = uuid();
        this.user = user;
        this.text = text;
        this.time = new Date();
    }
}
