import {useEffect, useState} from "react";
import {User} from "../model/user";
import {Message} from "../model/message";
import io from 'socket.io-client';
import {eventType} from "../event-type";
import {UserConnectedMessage} from "../model/user-connected-message";
import {UserDisconnectedMessage} from "../model/user-disconnected-message";
import {Chat} from "../model/chat";
import {PluginManager} from "../plugin/plugin-manager";
import {ShowTimestamp} from "../plugin/show-timestamp";
import {ShowUserInfo} from "../plugin/show-user-info";
import {AddAvatarUserList} from "../plugin/add-avatar-user-list";

const socket = io();

const emit = (type, payload) => {
    socket.emit('event', {type, payload});
};

export function useChatStore(initialState = []) {
    const [chats, setChats] = useState(initialState);
    const [events, setEvents] = useState([]);
    const [activeUsers, setActiveUsers] = useState([]);
    const [connectedUsers, setConnectedUsers] = useState([]);

    const handleEvent = (type, payload) => {
        console.log(type, payload);
        switch (type) {
            case eventType.MESSAGE_SENT:
                setEvents([...events, payload.message]);
                break;
            case eventType.USER_CONNECTED:
                setEvents([...events, new UserConnectedMessage(payload.user)]);
                setConnectedUsers(payload.connectedUsers);
                break;
            case eventType.USER_DISCONNECTED:
                setEvents([...events, new UserDisconnectedMessage(payload.user)]);
                setConnectedUsers(payload.connectedUsers);
                break;
            case eventType.USER_ACTIVITY_CHANGE:
                setActiveUsers(payload.users);
                break;
            default:
                break;
        }
    };

    useEffect(() => {
        const onEvent = ({type, payload}) => {
            handleEvent(type, payload);
        };
        socket.on('event', onEvent);
        return () => {
            socket.off('event', onEvent);
        }
    });

    const createChat = (name) => {
        const user = new User(name);
        emit(eventType.USER_CONNECTED, {user});
        const chat = new Chat(
            user,
            new PluginManager([
                new ShowTimestamp(),
                new ShowUserInfo(),
                new AddAvatarUserList(),
            ])
        );
        setChats(
            [...chats, chat]
        );
    };

    const removeChat = (id) => {
        const chat = chats.find((chat) => chat.id === id);
        if (chat) {
            setChats(chats.filter((chat) => chat.id !== id));
            emit(eventType.USER_DISCONNECTED, {user: chat.user});
        }
    };

    const addMessage = (user, text) => {
        const message = new Message(user, text);
        emit(eventType.MESSAGE_SENT, {message});
    };

    const onUserActivity = (user) => {
        emit(eventType.USER_TYPING, {user});
    };

    const heartBeat = (usersIds) => {
        emit(eventType.USER_HEART_BEAT, {usersIds});
    };

    const onPluginToggle = (chat, plugin) => {
        chat.pluginManager.toggle(plugin);
        console.log(chat.pluginManager);
        setChats([...chats]);
    };

    return {
        chats,
        removeChat,
        createChat,
        addMessage,
        messages: events,
        activeUsers,
        onUserActivity,
        connectedUsers,
        heartBeat,
        onPluginToggle
    };
}
